Jeśli nie działają pliki run.bat (lub jeszcze ich nie ma) to projekt należy zaimportować do IntelliJ

Otwórz IntelliJ
File->Open -> Wybierz folder Bank.

Jeśli po załadowaniu brakuje SDK należy podpiąć w project structure -> project (werjsa 1.8)

Jeśli nie widać modułów w projekcie to należy zamiast Open kliknąć Import Project (Bank)

Jak już uda się to skonfigurować i pojawi się zielona ikonka (taki play) możemy spróbować odpalić konfigurację BankMain.

Jeśli wyskoczy błąd o nieskonfiguorwanym Mavenie należy wejść w Settings -> Build Tools -> Maven
i ustawić zmienną Maven Home Directory - należy wskazać folder załączony do repozytorium apache-maven-3.2.5

Przebuduj projekt

Run BankMain

Teraz uruchamiasz po raz pierwszy może wyskoczyć błąd dostępu (zablokowany adres IP w bazie danych),
jeśli nie - można testować.