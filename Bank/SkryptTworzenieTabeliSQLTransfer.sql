create table Transfer (
   id INT NOT NULL identity,
   accountNumberFrom VARCHAR(26) default NULL,
   accountNumberTo VARCHAR(26) default NULL,
   title VARCHAR(255) default NULL,
   transferDate DATE default null,
   dollarCount     DOUBLE PRECISION  default 0,
   isRealized BIT default 0,
   isScheduled BIT default 0,
   scheduleDay     INT  default NULL,
   PRIMARY KEY (id)
);