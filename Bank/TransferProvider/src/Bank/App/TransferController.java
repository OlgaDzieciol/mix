package Bank.App;

import Bank.App.Contracts.*;
import Bank.App.Models.Account;
import Bank.App.Models.InsufficientFundsException;
import Bank.App.Models.Transfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by olga on 2014-12-30.
 */
@Component
public class TransferController implements ITransfer {
    @Autowired
    IAccount accContext;
    @Autowired
    IHistory transferContext;
    @Autowired
    IUserData userContext;
    @Autowired
    ICommunicator mailer;
    @Autowired
    IDocConverter konwerter;
    public TransferController()    {
        accContext = new DBController();
        transferContext = new DBController();
    }

    @Override
    public void Transfer(Transfer transfer) throws InsufficientFundsException {

        Account from = accContext.getAccount(transfer.getAccountNumberFrom().toString());

        //Transfer transfer = new Transfer(from.getAccountNumber(),to.getAccountNumber(),new Date(System.currentTimeMillis()),"przelew testowy",100,false,false,null);
        if (from.getBalance() > transfer.getDollarCount())
        {
            accContext.updateAccountBalance(from.getId(), from.getBalance() - transfer.getDollarCount());
            transferContext.addTransfer(transfer);
        }
        else{
            throw new InsufficientFundsException();
        }
    }

    @Override
    public boolean ScheduleTransfer(Transfer transfer, int day) {
        transfer.setisRealized(false);
        transfer.setScheduleDay(day);
        transfer.setisScheduled(true);
        transferContext.addTransfer(transfer);
        return true;
    }

    @Override
    public boolean RemoveScheduledTransfer(int transferId) {
        transferContext.deleteTransfer(transferId, true);
        return true;
    }

    @Override
    public List<Transfer> getTransferHistory(String accountNumber)
    {
       return transferContext.getTransferHistory(accountNumber);

    }
    private void RealizeTransfer(Transfer transfer)
    {
        System.out.print("Rozpoczecie realizacji pojedynczego przelewu\n");
        Account to = accContext.getAccount(transfer.getAccountNumberTo());
        accContext.updateAccountBalance(to.getId(), to.getBalance() + transfer.getDollarCount());
        transfer.setisRealized(true);
        transferContext.updateTransfer(transfer);
        Account from = accContext.getAccount(transfer.getAccountNumberFrom());
        String mail = userContext.getUser(from.getUserId()).getEmail();
        try {
            System.out.print("Wysylanie potwierdzenia mailem : \n" + mail.toString());
            List<AttachmentDetails> list = new ArrayList<AttachmentDetails>();
            list.add(konwerter.getMailDocument(transfer));
            mailer.sendEmail("wirtualnybank", mail, "Potwierdzenie przelewu", "Zrealizowaliśmy przelew, potwierdzenie w załączniku", list);

        }catch(javax.mail.MessagingException e)
        {
            System.out.print("Blad mailingu\n");
        }
    }
    private void CheckBeforeRealize(Transfer transfer)
    {
        Account from = accContext.getAccount(transfer.getAccountNumberFrom());
        if (from.getBalance() > transfer.getDollarCount())
        {
            RealizeTransfer(transfer);
        }
        else{
            String mail = userContext.getUser(from.getUserId()).getEmail();
            try {
                mailer.sendEmail("wirtualnybank", mail, "Nieudana realizacja", "Niestety nie udało nam się zrealizować przelewu z powodu braku środków na koncie", null);
            }catch(Exception e)
            {
                System.out.print("Bład w realizacji zaplanowanych" + e.toString() + "\n");
            }
        }
    }

    private void addScheduledToRealisation(Date date){
        System.out.print("Dodawanie zaplanowanych przelewow \n");
        List<Transfer> list = transferContext.getScheduledTransfers(date.getDay());
        for(Transfer transfer : list){
            transfer.setisScheduled(false);
            transferContext.addTransfer(transfer);
        }
    }
    @Override
    public void realizeTransfersForDay(Date date){
        System.out.print("Wejście do metody realizacji\n");
        addScheduledToRealisation(date);

        List<Transfer> list = transferContext.getNonRealizedTransfers();

        System.out.print("Znaleziono " + list.size() + " przelewow \n");
        for( Transfer transfer : list){
            if (transfer.getisScheduled())
            {
                CheckBeforeRealize(transfer);
            }
            else{
                RealizeTransfer(transfer);
            }
        }
    }



}
