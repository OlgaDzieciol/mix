package Bank.App;

import Bank.App.Contracts.AttachmentDetails;
import Bank.App.Contracts.IDocConverter;
import Bank.App.Contracts.InvalidDocumentFormatException;
import Bank.App.Models.Transfer;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import javax.mail.internet.MimeBodyPart;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * Created by olga on 2014-12-30.
 */
@Component
public class DocConverterImpl implements IDocConverter {
    MyXMLtoPDFPrinter printer;

    public DocConverterImpl()
    {
        printer = new MyXMLtoPDFPrinter();
    }

    public String readMeFirst()
    {
        return "Obecnie metoda XMLToPDF jest zoptymalizowana pod czat i działa poprawnie z konkretnym schematem documentu. Przykład w załączniku";
    }


    @Override
    public PDDocument XMLToPDF(File XMLDocument, String outputPath) throws InvalidDocumentFormatException {
        Document doc = buildDocumentFromFile(XMLDocument);
        PDDocument outputPDF;

        try
        {
            outputPDF = printer.PrintXMLToPDF(doc, outputPath);
            return outputPDF;
        }
        catch (Exception e)
        {
            System.out.println("Error in main method : " + e);
        }
        return null;

    }

    @Override
    public AttachmentDetails getMailDocument(Transfer transfer) {
        AttachmentDetails details = new AttachmentDetails();
        MyXMLtoPDFPrinter printer = new MyXMLtoPDFPrinter();
        File attachmentPDF = null;
        details.attachmentBody = new MimeBodyPart();
        try {
            attachmentPDF = printer.PrintTransfertoFILEPDF(transfer);
        }catch (Exception e){
            System.out.print("Blad tworzenia PDF");
        }

        try {
            details.attachmentBody.attachFile(attachmentPDF);
            //details.fileName = "lalala";
        }catch(Exception e){
            System.out.println(e);
        }


        return details;
    }

    static Document buildDocumentFromFile(File inputFile)
    {
        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);

            //optional, but recommended
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public PDDocument documentDlaBanku(){

        //Metoda Testowa
        PDDocument outputPDF = null;

        try
        {
            outputPDF = printer.PrintXMLToPDF(MyXMLtoPDFPrinter.ReadFile("/Users/Jan/Desktop/conversation.xml"),"/Users/Jan/Desktop/testowy.pdf");

        }
        catch (Exception e)
        {
            System.out.println("Error in main method : " + e);
        }
        return outputPDF;
    }

    public Document dokument(){
        return MyXMLtoPDFPrinter.ReadFile("/Users/Jan/Desktop/conversation.xml");//Metoda Testowa
    }








}
