package Bank.App;

import Bank.App.Models.Transfer;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;

/**
 * Created by Jan on 2015-01-02.
 */
 class MyXMLtoPDFPrinter {


    public static Document ReadFile(String path)
    {
        try {

            File fXmlFile = new File(path);//"/Users/Jan/Desktop/conversation.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            //optional, but recommended
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;


    }
    public static boolean isNewPageNeeded( int actualRowCount, int msgLetterRows, int maxRowsPerPage)
    {
        //Check czy na nową wiadomość wystarczy miejsca na stronie
        if (actualRowCount + msgLetterRows>= maxRowsPerPage)
        {
//            try{}
//            catch (Exception e)
//            {}
//            finally {
//                actualRowCount = 0;
//            }

            return true;
        }
        else
        {
            return false;
        }
    }
    public PDDocument PrintXMLToPDF(Document doc, String outputPath) throws IOException, COSVisitorException {
// Create a document and add a page to it
        PDDocument document = new PDDocument();
        PDPage page = new PDPage(PDPage.PAGE_SIZE_LETTER);
        document.addPage( page );
        PDPage newPage;
// Create a new font object selecting one of the PDF base fonts
        PDFont font = PDType1Font.getStandardFont("Arial");
        //PDFont font = PDTrueTypeFont.loadTTF(document, new File("/Users/Jan/Desktop/polishfonts.ttf"));


// Start a new content stream which will "hold" the to be created content
        PDPageContentStream contentStream = new PDPageContentStream(document, page);

        contentStream.beginText();
        contentStream.setFont(font, 12);
        contentStream.moveTextPositionByAmount(70, 750);
        contentStream.drawString("Conversation archive");
        System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

//Set up content borders
        int maxRowLength = 80;
        int maxRowsPerPage = 30;
        int actualRowCount = 0;


        NodeList nList = doc.getElementsByTagName("message");
        for (int i = 0; i < nList.getLength(); i++) {

            Node nNode = nList.item(i);


            if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) nNode;
                String msgValue = eElement.getElementsByTagName("value").item(0).getTextContent();
                int msgLettersRows = 1 + msgValue.length() / maxRowLength;
                int charFrom = 0;
                int charTo = maxRowLength;
                if(isNewPageNeeded(actualRowCount, msgLettersRows, maxRowsPerPage)) {
                    actualRowCount = 0;
                    contentStream.endText();
                    contentStream.close();
                    page = new PDPage(PDPage.PAGE_SIZE_LETTER);
                    document.addPage(page);

                    contentStream = new PDPageContentStream(document, page);
                    contentStream.beginText();
                    contentStream.setFont(font, 12);
                    contentStream.moveTextPositionByAmount(70, 750);
                }

                contentStream.moveTextPositionByAmount( 0, -20 );
                actualRowCount++;


                contentStream.drawString(eElement.getElementsByTagName("sender").item(0).getTextContent() + ", sent : " + eElement.getElementsByTagName("time").item(0).getTextContent());

                contentStream.moveTextPositionByAmount( 20, -20 );
                actualRowCount++;

                for (int j = 0 ; j < msgLettersRows; j++)
                {
                    if ( msgValue.length() < charTo)
                        charTo = msgValue.length();
                    if (msgValue.length() < charFrom)
                        charFrom = msgValue.length();
                    String msgRow = msgValue.substring(charFrom, charTo);

                    contentStream.drawString(msgRow);
                    contentStream.moveTextPositionByAmount( 0, -20 );
                    actualRowCount++;

                    charFrom+=maxRowLength;
                    charTo+=maxRowLength;
                }
                contentStream.moveTextPositionByAmount( -20,0 );
            }
        }
        contentStream.endText();


// Make sure that the content stream is closed:
        contentStream.close();

// Save the results and ensure that the document is properly closed:
        document.save(outputPath);

        document.close();
        return document;
    }

    public File PrintTransfertoFILEPDF(Transfer transfer) throws IOException, COSVisitorException {

        // Create a document and add a page to it
        PDDocument document = new PDDocument();
        PDPage page = new PDPage(PDPage.PAGE_SIZE_LETTER);
        document.addPage( page );
        PDPage newPage;
// Create a new font object selecting one of the PDF base fonts
        PDFont font = PDType1Font.getStandardFont("Arial");
        //PDFont font = PDTrueTypeFont.loadTTF(document, new File("/Users/Jan/Desktop/polishfonts.ttf"));


// Start a new content stream which will "hold" the to be created content
        PDPageContentStream contentStream = new PDPageContentStream(document, page);

        contentStream.beginText();
        contentStream.setFont(font, 12);
        contentStream.moveTextPositionByAmount(70, 750);
        contentStream.drawString("Potwierdzenie przelewu");
        contentStream.moveTextPositionByAmount( 0, -20 );
        contentStream.drawString("Z rachunku o numerze : " + transfer.getAccountNumberFrom());
        contentStream.moveTextPositionByAmount( 0, -20 );
        contentStream.drawString("Na rachunek : " + transfer.getAccountNumberTo());
        contentStream.moveTextPositionByAmount( 0, -20 );
        contentStream.drawString("Tytul przelewu : " + transfer.getTitle());
        contentStream.moveTextPositionByAmount( 0, -20 );
        contentStream.drawString("Kwota przelewu : " + transfer.getDollarCount());
        contentStream.moveTextPositionByAmount( 0, -20 );
        contentStream.drawString("Data realizacji : " + transfer.getTransferDate().toString());
        contentStream.moveTextPositionByAmount( 50, -20 );
        contentStream.drawString("Pozdrawia Twoj Wirtualny Bank");
        contentStream.endText();

// Make sure that the content stream is closed:
        contentStream.close();

// Save the results and ensure that the document is properly closed:
        document.save("PotwierdzeniePrzelewu.pdf");

        document.close();
        File pdf = new File("PotwierdzeniePrzelewu.pdf");
        return pdf;
    }
 }

