package Bank.App;

import Bank.App.Contracts.IPaniZOkienka;
import Bank.App.Models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Date;
import java.util.List;


/**
 * Created by Olga on 2015-01-12.
 */
@Component
public class BankGUI {

    @Autowired
    private IPaniZOkienka panienka;
    private BankUser loggedUser;
    List<Account> accountsList;
    Card card;
    public BankGUI() {
    }

    public void Check() {
        if (panienka != null) {
            System.out.println("jest nie null");

        } else {
            System.out.print("skucha");
        }
    }

    public void Bank() {

        //początek aplikacji konsolowej

        powitanie();

        char akcja;
        akcja = 'x';

        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));

        while (akcja != 'q') {
            pierwszyWybor();
            System.out.print("");

            try {
                akcja = buffer.readLine().charAt(0);
                switch (akcja) {
                    case '1': {
                        this.zaloguj();
                        break;
                    }
                    case '2': {
                        this.rejestruj();
                        break;
                    }
                    case '3': {
                        panienka.WymusRealizacjePrzelewow();
                        break;
                    }
                    case 'q': {
                        System.out.print("Do widzenia! \n \n");
                        break;
                    }

                    default: {
                        System.out.print("błędny znak! \n \n");
                    }

                }
            } catch (Exception e) {
                System.err.println("Błąd! " + e.toString() + "\n \n");
            }


        }


    }

    static void powitanie() {
        System.out.print("Witamy w banku! ");
    }

    static void pierwszyWybor() {
        System.out.print("Naciśnij: " + "\n -'1' jeżeli posiadasz konto w naszym banku i chcesz się zalogować " + "\n-'2' jeżeli nie masz konta i chcesz się zarejestrować" + "\n -'q' jeżeli chcesz wyjść \n");
    }
    static void trescBankMenu() {
        System.out.print("Naciśnij: " + "\n -'1' Pokaz Moje konta " + "\n-'2' Zrob przelew" + "\n -'9' Wyloguj \n");
    }

    void zaloguj() {
        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("podaj login: \n");
            String login = buffer.readLine();

            System.out.print("\n podaj hasło: \n");
            String haslo = buffer.readLine();

            if (panienka.Zaloguj(login, haslo)) {
                loggedUser = panienka.getUsername(login);
                accountsList =  panienka.wyswietlKonta(loggedUser.getId());
                menuGlowne();

            } else {
                System.out.print("błędny login lub hasło!");
                zaloguj();
            }
        } catch (Exception e) {
            System.err.println("wystąpił błąd! " + e.getMessage() + "\n \n");
        }
    }

//
    void rejestruj(){
        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Jaki chcesz miec login ? : \n");
            String login = buffer.readLine();

            System.out.print("\n Jakie chcesz miec haslo ? : \n");
            String haslo = buffer.readLine();

            System.out.print("\n Podaj swoj email : \n");
            String email = buffer.readLine();

            loggedUser = panienka.Rejestruj(login,email,haslo);

            if (loggedUser!= null) {

                System.out.print("Na potrzeby testowe zostanie zalozone konto glowne z domyslna kwota 1000 \n");
                panienka.ZalozKonto(loggedUser.getId(),0,1000,null,0);
                accountsList = panienka.wyswietlKonta(loggedUser.getId());

                menuGlowne();

            } else {
                System.out.print("Podany email lub login istnieje juz w naszej bazie. Sprobuj ponownie \n ");
                rejestruj();
            }
        } catch (Exception e) {
            System.err.println("wystąpił błąd! " + e.getMessage() + "\n \n");
        }
    }
void menuGlowne()
{

    char akcja2;
    akcja2 = 'x';

    BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
    while (akcja2 != 'w') {
        System.out.print("\nBANK \n-1- menu wiadomoœci\n-2- menu profil\n" +
                "-3- menu rachunki\n-4- menu oszczêdnoœci\n-5- menu kredyty\n" +
                "-6- menu karty\n-w- wyloguj\n");
        try {
            akcja2 = buffer.readLine().charAt(0);
            switch (akcja2) {
                case '1': {
                    wiadomosciMenu();
                    break;
                }
                case '2': {
                    profilMenu();
                    break;
                }
                case '3': {
                    rachunekMenu();
                    break;
                }
                case '4': {
                    oszczednosciMenu();
                    break;
                }
                case '5': {
                    kredytyMenu();
                    break;
                }
                case '6': {
                    kartyMenu();
                    break;
                }
                case 'w': {
                    break;
                }
                default: {
                    System.out.print("Zly wybor opcji \n \n");
                    break;
                }
            }
        } catch (Exception e) {
            System.err.println("Blad : " + e.getMessage() + "\n \n");
        }
    }
}

    void wiadomosciMenu()
    {
        System.out.print("\nWIADOMOŒCI MENU\n-1- nowa wiadomoœæ\n-2- odebrane wiadomoœci\n-3- wys³ane wiadomoœci\n-b- wróæ\n");

        char akcja3;

        BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));

        try{
            akcja3 = buffer.readLine().charAt(0);

            switch (akcja3)
            {
                case '1':
                {
                    nowaWiadomosc();
                    break;
                }
                case '2':
                {
                    odebraneWiadomosci();
                    break;
                }
                case '3':
                {
                    wyslaneWiadomosci();
                    break;
                }
                case 'b':
                {

                    break;
                }
                default:
                {
                    System.out.print("b³êdny znak! \n \n");
                }
            }
        }

        catch  (Exception e)
        {
            System.err.println("b³êdny znak! " + e.getMessage() + "\n \n");
        }
    }

    void nowaWiadomosc()
    {
        System.out.print("\nNOWA WIADOMOŒÆ\n");
        //TODO po wejœciu w 'nowa wiadomoœæ' nie ma jak sie cofn¹æ
        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
            int id = loggedUser.getId();

            System.out.print("Temat wiadomoœci : \n");
            String temat= buffer.readLine();

            System.out.print("\nTreœæ wiadomoœci : \n");
            String trescWiadomosci = buffer.readLine();

            panienka.Wyslij(temat,trescWiadomosci,id);
            // TODO mo¿emy zrobiæ, ¿eby Wyœlij zwracalo true albo false? wtedy tutaj:
            //if (panienka.Wyslij(temat,trescWiadomosci,id) {
            // System.out.print("\nWiadomoœæ zosta³a wys³ana pomyœlnie. \n");
            //wiadomosciMenu();

            //} else {
            //System.out.print("\nWyst¹pi³ b³¹d, wysy³anie wiadomoœci nie powiod³o siê. \n");
            //wiadomosciMenu();
            //}

        } catch (Exception e) {
            System.err.println("wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
        }
    }

    void odebraneWiadomosci()
    {
        System.out.print("\nODEBRANE WIADOMOŒCI\n");
        String typ = "odebrane";
        panienka.Wyswietl(typ, loggedUser.getId());
        wiadomosciMenu();

    }

    void wyslaneWiadomosci()
    {
        System.out.print("\nWYS£ANE WIADOMOŒCI\n");
        String typ = "wyslane";
        panienka.Wyswietl(typ, loggedUser.getId());
        wiadomosciMenu();
    }

    void profilMenu()
    {


        char akcja4;
        akcja4 = 'x';
        BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));
    while (akcja4!='b'){
        System.out.print("MENU PROFIL: \n-1- edytuj dane \n-2- zmieñ has³o \n-3-Pokaz moje dane  \n-b- cofnij\n");
        try{
            akcja4 = buffer.readLine().charAt(0);

            switch (akcja4)
            {
                case '1':
                {
                    edytujDane();
                    break;
                }
                case '2':
                {
                    zmienHaslo();
                    break;
                }
                case '3':
                {
                    pokazDane();
                    break;
                }

                case 'b':
                {

                    break;
                }
                default:
                {
                    System.out.print("b³êdny znak! \n \n");
                }
            }

        }
        catch  (Exception e)
        {
            System.err.println("wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
        }
    }}

    private void pokazDane() {
        System.out.print("Nazwa uzytkownika : " + loggedUser.getUsername() + "\n" );
        System.out.print("Imie i nazwisko : " + loggedUser.getFirstname() + " " + loggedUser.getLastname() + "\n");
        System.out.print("Data urodzenia : " + loggedUser.getBirthDate() + "\n" );
        System.out.print("Pesel : " +  loggedUser.getPesel() + "\n" );
        System.out.print("Adres kontaktowy : " + loggedUser.getEmail() + "\n" );
    }

    void edytujDane()
    {


        char akcja5;
        akcja5 = 'x';
        BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));
        while (akcja5!='b') {
            System.out.print("EDYTUJ DANE\n-1- zmieñ imiê \n-2- zmieñ nazwisko \n-3- zmieñ login \n-4- zmieñ e-mail \n-b- cofnij \n");
            try {
                akcja5 = buffer.readLine().charAt(0);

                switch (akcja5) {
                    case '1': {
                        zmienImie();
                        break;
                    }
                    case '2': {
                        zmienNazwisko();
                        break;
                    }
                    case '3': {
                        zmienLogin();
                    }
                    case '4': {
                        zmienEmail();
                    }
                    case 'b': {
                        //menuGlowne();
                        break;
                    }
                    default: {
                        System.out.print("b³êdny znak! \n \n");
                    }
                }
            } catch (Exception e) {
                System.err.println("wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
            }
        }
        panienka.EdytujDane(loggedUser);
    }

    void zmienImie()
    {
        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("\nPodaj imiê: \n");
            String imie = buffer.readLine();

            String stareimie = loggedUser.getFirstname();

            loggedUser.setFirstname(imie);
            if (stareimie.equals(imie)) {
                System.out.print("Wyst¹pi³ b³¹d.");

            } else {
                System.out.print("Dane zosta³y zmienione.");

            }
        } catch (Exception e) {
            System.err.println("Wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
        }
    }

    void zmienNazwisko()
    {
        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("\nPodaj nazwisko: \n");
            String nazwisko = buffer.readLine();

            String starenazwisko = loggedUser.getLastname();

            loggedUser.setLastname(nazwisko);
            if (starenazwisko.equals(nazwisko)) {
                System.out.print("Wyst¹pi³ b³¹d.");

            } else {
                System.out.print("Dane zosta³y zmienione.");

            }
        } catch (Exception e) {
            System.err.println("Wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
        }
    }

    void zmienLogin()
    {
        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Podaj login: \n");
            String login = buffer.readLine();

            String starylogin = loggedUser.getUsername();

            loggedUser.setUsername(login);
            if (starylogin.equals(login)) {
                System.out.print("Wyst¹pi³ b³¹d.");
                edytujDane();
            } else {
                System.out.print("Dane zosta³y zmienione.");
                edytujDane();
            }
        } catch (Exception e) {
            System.err.println("Wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
        }
    }

    void zmienEmail()
    {
        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Podaj email: \n");
            String email = buffer.readLine();

            String staryemail = loggedUser.getEmail();

            loggedUser.setEmail(email);

            if (staryemail.equals(email)) {
                System.out.print("Wyst¹pi³ b³¹d.");
                edytujDane();
            } else {
                System.out.print("Dane zosta³y zmienione.");
                edytujDane();
            }
        } catch (Exception e) {
            System.err.println("Wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
        }
    }

    void noweHaslo()
    {
        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("\nPodaj nowe has³o: \n");
            String nowehaslo = buffer.readLine();
            System.out.print("\nPowtórz nowe has³o: \n");
            String nowehaslo2 = buffer.readLine();

            if (nowehaslo.equals(nowehaslo2))
            {
                loggedUser.setPassword(nowehaslo);
                System.out.print("Dane zosta³y zmienione.");
                profilMenu();
            }

            else
            {
                System.out.print("\nHas³a s¹ ró¿ne! \n");
                noweHaslo();
            }
        }

        catch (Exception e) {
            System.err.println("Wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
        }

    }

    void zmienHaslo()
    {
        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("\nPodaj stare has³o: \n");
            String starehaslo = buffer.readLine();

            String haslou =loggedUser.getPassword();
            if (starehaslo.equals(haslou))
            {
                noweHaslo();
            }
            else
            {
                System.out.print("Poda³eœ b³êdne has³o!");
                zmienHaslo();
            }

        } catch (Exception e) {
            System.err.println("Wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
        }
    }

    void rachunekMenu()
    {


        char akcja6;
        akcja6 = 'x';
        BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));
        while(akcja6 != 'b'){
            System.out.print("RACHUNEK MENU: \n-1- informacje o kontach \n-2- przelewy \n-3- historia \n-b- cofnij");
        try{
            akcja6 = buffer.readLine().charAt(0);

            switch (akcja6)
            {
                case '1':
                {
                    stankonta();
                    break;
                }
                case '2':
                {
                    przelewyMenu();
                    break;
                }
                case '3':
                {
                    historia();
                }
                case 'b':
                {

                    break;
                }
                default:
                {
                    System.out.print("b³êdny znak! \n \n");
                }
            }
        }
        catch  (Exception e)
        {
            System.err.println("wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
        }}
    }

    void stankonta()
    {
        System.out.print("\nTwoje konta : \n");

        for(Account acc : accountsList){
            System.out.print("Id konta : " + acc.getId() + "\n");
            System.out.print("Numer konta : " + acc.getAccountNumber() + "\n");
            System.out.print("Stan konta : " + acc.getBalance() + "\n");
            System.out.print("Typ konta : " + acc.getAccountType() + "\n");
        }

    }

    void przelewyMenu()
    {


        char akcja7;
        akcja7 = 'x';
        BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));
        while (akcja7!='b'){
            System.out.print("\nPRZELEWY MENU: \n-1- zrób przelew \n-2- zlecenia sta³e \n-b- cofnij \n");
        try{
            akcja7 = buffer.readLine().charAt(0);

            switch (akcja7)
            {
                case '1':
                {
                    zrobPrzelew();
                    break;
                }
                case '2':
                {
                    zleceniaStale();
                    break;
                }
                case 'b':
                {

                    break;
                }
                default:
                {
                    System.out.print("b³êdny znak! \n \n");
                }
            }
        }
        catch  (Exception e)
        {
            System.err.println("wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
        }}
    }

    void zrobPrzelew()
    {
        BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));

        Account main = null;
        for(Account acc : accountsList){
            if (acc.getAccountType() == 0)
            {
                main = acc;
            }
        }
        if(main != null){
            System.out.print("\nPrzelew zostanie wykonany z Twojego glownego konta");
            System.out.print("\nStan twojego konta : "  + main.getBalance() );

        try {
            System.out.print("\nPodaj nr konta na ktory chcesz przeslac : " );
            String numberTo = buffer.readLine();
            System.out.print("Podaj kwote przelewu : " );

            double dollars = Double.parseDouble(buffer.readLine());
            System.out.print("Podaj tytuł przelewu  : " );
            String title = buffer.readLine();
            Transfer transfer = new Transfer(main.getAccountNumber(), numberTo,new Date(System.currentTimeMillis()),title,dollars,false,false,0);//,null,null)

            System.out.print(panienka.transfer(transfer));
            accountsList = panienka.wyswietlKonta(loggedUser.getId());

        }catch (Exception e){
            System.out.print("Błąd wprowadzania danych " + e );
            zrobPrzelew();

        }}
        else {
            System.out.print("\nNiestety nie znalezlismy Twojego glownego konta, sprobuj sie przelogowac");
        }
    }

    void zleceniaStale()
    {
        BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));

        Account main = null;
        for(Account acc : accountsList){
            if (acc.getAccountType() == 0)
            {
                main = acc;
            }
        }
        if(main != null){
            System.out.print("Przelew bedzie wykonanywany z Twojego glownego konta \n");


            try {
                System.out.print("Podaj nr konta na ktory chcesz przeslac : " );
                String numberTo = buffer.readLine();
                System.out.print("Podaj kwote przelewu : " );

                double dollars = Double.parseDouble(buffer.readLine());
                System.out.print("Podaj tytuł przelewu  : " );
                String title = buffer.readLine();
                int scheduleDay = 32;
                while(scheduleDay>31) {
                    System.out.print("Podaj dzień miesiąca w którym ma być realizowany przelew (niewiekszy od 31)  : ");
                    scheduleDay = Integer.parseInt(buffer.readLine());
                }
                Transfer transfer = new Transfer(main.getAccountNumber(), numberTo,new Date(System.currentTimeMillis()),title,dollars,false,true,scheduleDay);//,null,null)

                System.out.print(panienka.transfer(transfer));
                accountsList = panienka.wyswietlKonta(loggedUser.getId());

            }catch (Exception e){
                System.out.print("Błąd wprowadzania danych " + e );
                zleceniaStale();

            }}
        else {
            System.out.print("Niestety nie znalezlismy Twojego glownego konta, sprobuj sie przelogowac");
        }
    }

    void historia()
    {
        Account main = null;
        for(Account acc : accountsList){
            if (acc.getAccountType() == 0)
            {
                main = acc;
            }
        }
        System.out.print("\nTwoje konta konta: \n");


        if (main == null){
            System.out.print("Niestety nie masz jeszcze u nas konta \n");
        }
        else{
            List<Transfer> transfers = panienka.wyswietlPrzelewy(main.getAccountNumber().toString());
            if (transfers.size() == 0){
                System.out.print("Nie masz zadnych przelewow");
            }
            for(Transfer transfer : transfers){
                System.out.print("Nr konta odbiorcy : " + transfer.getAccountNumberTo() + "\n");
                System.out.print("Kwota przelewu : " + transfer.getDollarCount() + "\n");
                System.out.print("Tytuł przelewu : " + transfer.getTitle() + "\n");
                System.out.print("Czy zrealizowany : " + transfer.getisRealized() + "\n");
            }
        }


    }

    void oszczednosciMenu()
    {

        char akcja8;
        akcja8 = 'x';

        BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));
        while (akcja8!='b') {
            System.out.print("MENU OSZCZÊDNOŒCI: \n-1- rachunki oszczêdnoœciowe \n-2- lokaty  \n-b- wróæ\n");
            try {
                akcja8 = buffer.readLine().charAt(0);

                switch (akcja8) {
                    case '1': {
                        rachunkiOszczednosciowe();
                        break;
                    }
                    case '2': {
                        lokaty();
                        break;
                    }
//                    case '3': {
//                        kontoEmerytalne();
//                        break;
//                    }
                    case 'b': {

                        break;
                    }
                    default: {
                        System.out.print("b³êdny znak! \n \n");
                    }
                }
            } catch (Exception e) {
                System.err.println("wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
            }
        }
    }

    void rachunkiOszczednosciowe()
    {


        char akcja9;
        akcja9 = 'x';
        BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));
        while (akcja9!='b'){
            System.out.print("RACHUNKI OSZCZÊDNOŒCIOWE: \n-1- sprawdŸ stan rachunku oszczêdnoœciowego \n-2- za³ó¿ rachunek oszczêdnoœciowy \n-b- wróæ \n");
        try{
            akcja9 = buffer.readLine().charAt(0);

            switch (akcja9)
            {
                case '1':
                {
                    stanRachunkuOszczednosciowego();
                    break;
                }
                case '2':
                {
                    zalozRachunekOszczednosciowy();
                    break;
                }
                case 'b':
                {

                    break;
                }
                default:
                {
                    System.out.print("b³êdny znak! \n \n");
                }
            }
        }
        catch  (Exception e) {
            System.err.println("wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
        }
    }}

    void stanRachunkuOszczednosciowego()
    {
        System.out.print("\nSTAN RACHUNKU OSZCZÊDNOŒCIOWEGO: \n");
        for(Account acc : accountsList){
            if (acc.getAccountType() == 1) {
                System.out.print("Id konta : " + acc.getId() + "\n");
                System.out.print("Numer konta : " + acc.getAccountNumber() + "\n");
                System.out.print("Stan konta : " + acc.getBalance() + "\n");
                System.out.print("\nAby wyplacic pieniadze nalezy udac sie do siedziby banku :) \n");
            }
        }


    }

    void zalozRachunekOszczednosciowy()
    {
        System.out.print("\nZA£Ó¯ RACHUNEK OSZCZÊDNOŒCIOWY: \n");
        boolean hasAlready = false;
        for(Account acc : accountsList){
            if (acc.getAccountType() == 1) {
                hasAlready = true;

                System.out.print("Masz już rachunek oszczednosciowy, mozna miec maksymalnie jeden \n");
            }
        }
        if(!hasAlready) {
            panienka.ZalozKonto(loggedUser.getId(), 1, 0, loggedUser.getBirthDate(), 100);
            accountsList =  panienka.wyswietlKonta(loggedUser.getId());
            System.out.print("Zalozono rachunek osczednosciowy ! Aby wplacic na niego pieniadze sprawdz jego nr i zrob przelew");
        }
    }

    void lokaty()
    {

        char akcja10;
        akcja10 = 'x';

        BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));
        while (akcja10!='b'){
            System.out.print("LOKATY: \n-1- sprawdŸ stan lokaty \n-2- za³ó¿ lokatê \n-b- wróæ\n");
        try{
            akcja10 = buffer.readLine().charAt(0);

            switch (akcja10)
            {
                case '1':
                {
                    stanLokaty();
                    break;
                }
                case '2':
                {
                    zalozLokate();
                    break;
                }
                case 'b':
                {

                    break;
                }
                default:
                {
                    System.out.print("b³êdny znak! \n \n");
                }
            }
        }
        catch  (Exception e)
        {
            System.err.println("wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
        }}
    }

    void stanLokaty()
    {
        System.out.print("\nSTAN Lokat: \n");
        for(Account acc : accountsList){
            if (acc.getAccountType() == 2) {
                System.out.print("Id konta : " + acc.getId() + "\n");
                System.out.print("Numer konta : " + acc.getAccountNumber() + "\n");
                System.out.print("Stan konta : " + acc.getBalance() + "\n");

            }
        }
        System.out.print("\n Pieniadze zostana wyplacone po skonczeniu okresu trwania lokaty \n");
    }

    void zalozLokate()
    {
        System.out.print("\nZaloz lokate: \n");
        boolean hasAlready = false;
        for(Account acc : accountsList){
            if (acc.getAccountType() == 2) {
                hasAlready = true;

                System.out.print("Masz już lokatę \n");
            }
        }
        if(!hasAlready) {
            BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));

            double kwota = 0;
            try {
                kwota = Double.parseDouble(buffer.readLine());
                panienka.ZalozKonto(loggedUser.getId(), 2, kwota, loggedUser.getBirthDate(), 1);
                accountsList =  panienka.wyswietlKonta(loggedUser.getId());

            }catch (Exception e){
                System.out.print("Zła kwota");
                zalozLokate();
            }

            System.out.print("Zalozono lokate bezterminową! Kwota lokaty : " + kwota);
        }
    }

//    void kontoEmerytalne()
//    {
//
//        char akcja11;
//        akcja11 = 'x';
//
//        BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));
//        while(akcja11!='b') {
//            System.out.print("Zrezygnowalismy z implementacji kont emerytalnych \n");
//            System.out.print("KONTO EMERYTALNE:\n-1- sprawdŸ stan konta emerytalnego \n-2- za³ó¿ konto emerytalne \n-b- wróæ\n");
//            try {
//                akcja11 = buffer.readLine().charAt(0);
//
//                switch (akcja11) {
//                    case '1': {
//                        stanKontaEmerytalnego();
//                        break;
//                    }
//                    case '2': {
//                        zalozKontoEmerytalne();
//                        break;
//                    }
//                    case 'b': {
//                        oszczednosciMenu();
//                        break;
//                    }
//                    default: {
//                        System.out.print("b³êdny znak! \n \n");
//                    }
//                }
//            } catch (Exception e) {
//                System.err.println("wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
//            }
//        }
//    }

    void stanKontaEmerytalnego()
    {

    }

    void zalozKontoEmerytalne()
    {
    }

    void kredytyMenu()
    {

        char akcja12;
        akcja12 = 'x';

        BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));
        while (akcja12!='b'){

            System.out.print("\n KREDYTY MENU:\n-1- stan kredytu \n-2- weŸ kredyt \n-b- wróæ\n");
        try{
            akcja12 = buffer.readLine().charAt(0);

            switch (akcja12)
            {
                case '1':
                {
                    stanKredytu();
                    break;
                }
                case '2':
                {
                    wezKredyt();
                    break;
                }
                case 'b':
                {

                    break;
                }
                default:
                {
                    System.out.print("b³êdny znak! \n \n");
                }
            }
        }
        catch  (Exception e)
        {
            System.err.println("wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
        }
    }}

    void stanKredytu()
    {
        System.out.print("\nSTAN KREDYTU: ");
        Credit local = null;
        local  = panienka.getCredit(loggedUser.getId());
        if(local != null){
            System.out.print("\nPozostało do spłaty : " + local.getCreditLeft() );
        }else{
            System.out.print("\nNie masz jeszcze kredytu - mozesz go zaciagnac! :) ");
        }
    }

    void wezKredyt()
    {
        Credit local = null;
        local  = panienka.getCredit(loggedUser.getId());
        if(local != null){
            System.out.print("\nMozesz miec tylko jeden kredyt na raz! " );


        }else{
            System.out.print("\n Na jaka kwote chcialbys zaciagnac kredyt ?" );
            try {
                BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));
                double dollars = Double.parseDouble(buffer.readLine());
                Credit credit = new Credit(loggedUser.getId(),dollars);
                panienka.newCredit(credit);

            }catch (Exception e){
                System.out.print("Wystapil blad przy zaciaganiu kredytu " + e );
            }
        }
    }

    void kartyMenu()
    {

        char akcja13;
        akcja13 = 'x';

        BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));
        while(akcja13!='b') {
            System.out.print("\nKARTY MENU: \n-1- aktywuj kartê \n-2- zmieñ PIN \n-3- ustawienia limitów \n-4- zamów wirtualn¹ kartê \n-b- wróæ \n");
            try {
                akcja13 = buffer.readLine().charAt(0);

                switch (akcja13) {
                    case '1': {
                        aktywujarte();
                        break;
                    }
                    case '2': {
                        zmienPIN();
                        break;
                    }
                    case '3': {
                        ustawieniaLimitow();
                        break;
                    }
                    case '4': {
                        zamowKarte();
                        break;
                    }
                    case 'b': {

                        break;
                    }
                    default: {
                        System.out.print("b³êdny znak! \n \n");
                    }
                }
            } catch (Exception e) {
                System.err.println("wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
            }
        }
    }

    void aktywujarte()
    {
        System.out.print("AKTYWUJ KARTÊ \n");
        if(card !=null) {
            panienka.aktywujKarte(card.getCardNumber());
        }else {
            System.out.print("Nie masz jeszcze karty, wybierz wpierw 4 aby zamowic nowa \n");
        }

    }

    void zmienPIN()
    {
        System.out.print("ZMIEÑ PIN \n");

        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("\nPodaj nowy PIN: \n");
            int nowyPIN = Integer.parseInt(buffer.readLine());
            panienka.ZmienPIN(card.getCardNumber(),nowyPIN);

        } catch (Exception e) {
            System.err.println("Wyst¹pi³ b³¹d! " + e.getMessage() + "\n \n");
        }
    }

    void ustawieniaLimitow()
    {
        System.out.print("USTAWIENIA LIMITOW \n");


            BufferedReader buffer= new BufferedReader(new InputStreamReader(System.in));
            if(card!=null){
            try {
                System.out.print("Jaki chcesz miec dzienny limit ? : \n");
                double dollars = Double.parseDouble(buffer.readLine());
                panienka.ustawLimit(card.getCardNumber(),dollars);
            }catch (Exception e){
                System.out.print("Błąd wprowadzania danych " + e );
                ustawieniaLimitow();

            }}
        else{
                System.out.print("Nie masz karty");
            }


    }

    void zamowKarte()
    {
        System.out.print("Zamawiasz wirtualne karte \n");
        Account main = null;
        for(Account acc : accountsList){
            if (acc.getAccountType() == 0)
            {
                main = acc;
            }
        }
        card = panienka.zamowKarte(main.getAccountNumber());
        System.out.print("Udalo sie! Twoj nr karty to : \n" + card.getCardNumber());

    }


}



