package tests

import components.authentication.Authentication
import components.authentication.IAuthentication
import components.authentication.exceptions.UserAlreadyLoggedInException
import components.authentication.exceptions.UserDoesNotExistsException
import components.authentication.testimpl.FejkDatabase
import components.authentication.testimpl.RandomUser
import org.junit.Test

/**
 * Created by Mateusz on 2014-12-16.
 */
class AuthenticationTest extends GroovyTestCase {


    void "test: \"Should be able to register an account. \""() {
        FejkDatabase d = new FejkDatabase();
        IAuthentication authentication = new Authentication(d);
        assert authentication.register("Mateusz", "123456", "mpawlak62@gmail.com", null)
    }

    void "test: \"Should not be able to register second account with same email address\""() {
        FejkDatabase d = new FejkDatabase();
        IAuthentication authentication = new Authentication(d);
        authentication.register("Mateusz", "123456", "mpawlak62@gmail.com", null);
        assert authentication.register("Marian", "654321", "mpawlak62@gmail.com", null) == false
    }

    void "test: \"Should not be able to register second account with same user name\""() {
        FejkDatabase d = new FejkDatabase();
        IAuthentication authentication = new Authentication(d);
        authentication.register("Mateusz", "123456", "mpawlak62@gmail.com", null);
        assert authentication.register("Mateusz", "654321", "marian@gmail.com", null) == false
    }

    void "test: \"Should be able to log in\""() {
        FejkDatabase d = new FejkDatabase();
        d.addUser("Mateusz", "123456", "mpawlak62@gmail.com", null);
        IAuthentication authentication = new Authentication(d);
        assert authentication.login("Mateusz", "123456")
    }


    void "test: \"Should not be able to login into an account on which someone is already logged in\""() {
        FejkDatabase d = new FejkDatabase();
        d.addUser("Mateusz", "123456", "mpawlak62@gmail.com", null);

        IAuthentication authentication = new Authentication(d);
        IAuthentication authentication2 = new Authentication(d);
        authentication.login("Mateusz", "123456");
        shouldFail(UserAlreadyLoggedInException) {
            authentication2.login("Mateusz", "123456");
        }
    }

    void "test: \"Should not be able to log in into an account that was not registered\""() {
        FejkDatabase d = new FejkDatabase();
        IAuthentication authentication = new Authentication(d);

        shouldFail(UserDoesNotExistsException) {
            authentication.login("Marian", "wiosło");
        }
    }

    void "test: \"Should be able to log in then log out and again log in(same account)\""() {
        FejkDatabase d = new FejkDatabase();
        IAuthentication authentication = new Authentication(d);
        authentication.register("Mateusz", "123456", "mpawlak62@gmail.com", null);
        authentication.login("Mateusz", "123456")
        authentication.logout();
        assert authentication.login("Mateusz", "123456")
    }

    void "test: \"After logged in, should be able to check if is logged in and get positive answer\""() {
        FejkDatabase d = new FejkDatabase();
        IAuthentication authentication = new Authentication(d);
        authentication.register("Mateusz", "123456", "mpawlak62@gmail.com", null);
        authentication.login("Mateusz", "123456")
        assert authentication.isUserLoggedIn()
    }

    void "test: \"Should be able to get correct user name\""() {
        FejkDatabase d = new FejkDatabase();
        IAuthentication authentication = new Authentication(d);
        authentication.register("Mateusz", "123456", "mpawlak62@gmail.com", null);
        authentication.login("Mateusz", "123456")
        assert authentication.getUserName().equals("Mateusz")
        authentication.logout();
        authentication.register("Michał", "1234", "Michał@d.pl", null);
        authentication.login("Michał", "1234")
        assert authentication.getUserName().equals("Michał")
        authentication.logout();
    }

    void "test: \"Should be able to get correct authorities\""() {
        FejkDatabase d = new FejkDatabase();
        IAuthentication authentication = new Authentication(d);
        authentication.register("Mateusz", "123456", "mpawlak62@gmail.com", null);
        authentication.login("Mateusz", "123456")
        assert authentication.getUserAuthorities()[0].getAuthority().equals("user")
        assert authentication.isUserInRole(new RandomUser())

    }

    void "test: \"Should be able to get remaining session time correctly.   \""()
    {
        FejkDatabase d = new FejkDatabase();
        IAuthentication authentication = new Authentication(d);
        authentication.register("Mateusz", "123456", "mpawlak62@gmail.com", null);
        authentication.login("Mateusz", "123456")
        Long t1 = authentication.getRemainingSessionTime();
        sleep(100);
        Long t2 = authentication.getRemainingSessionTime();
        assert t1 > t2
    }

    void "test: \"Should be logged out after given time. \""() {
        FejkDatabase d = new FejkDatabase();
        IAuthentication authentication = new Authentication(d, 100);
        authentication.register("Mateusz", "123456", "mpawlak62@gmail.com", null);
        authentication.login("Mateusz", "123456")
        sleep(100);
        assert !authentication.isUserLoggedIn()
    }
}
