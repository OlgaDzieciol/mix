package components.authentication.exceptions;


/**
 * Created by Mateusz on 2014-12-12.
 */
class LoginException extends Exception {
    public LoginException() {

    }
    public LoginException(String message) {
        super(message);
    }
}
