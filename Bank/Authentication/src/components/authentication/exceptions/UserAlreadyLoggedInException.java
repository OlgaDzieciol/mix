package components.authentication.exceptions;

/**
 * Created by Mateusz on 2014-12-12.
 */
public class UserAlreadyLoggedInException extends LoginException {
    public UserAlreadyLoggedInException(String message) {
        super(message);
    }

    public UserAlreadyLoggedInException() {

    }
}
