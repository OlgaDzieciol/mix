package components.authentication.exceptions;

/**
 * Created by Mateusz on 2014-12-12.
 */
public class UserDoesNotExistsException extends LoginException {
    public UserDoesNotExistsException() {

    }

    public UserDoesNotExistsException(String message) {
        super(message);
    }
}
