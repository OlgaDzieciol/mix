package components.authentication.testimpl;

import components.authentication.IUserData;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.*;

/**
 * Created by Mateusz on 2014-12-12.
 */
public class FejkDatabase implements IUserData {

    private HashMap<String, User> sessions = new HashMap<String, User>();
    private HashMap<String, Date> sessionTiming = new HashMap<String, Date>();
    private HashMap<String, String> userSessionMap = new HashMap<String, String>();


    // Registered.
    private HashSet<String> users = new HashSet<String>();
    private HashSet<String> emails = new HashSet<String>();
    private HashMap<String, User> userNameUser = new HashMap<String, User>();


    @Override

    public boolean userExists(String userName) {
        return users.contains(userName);
    }

    @Override
    public boolean emailUsed(String email) {
        return emails.contains(email);
    }

    @Override
    public User getUserBySession(String sessionKey) {
        if (!sessions.containsKey(sessionKey)) {
            return null;
        }
        return sessions.get(sessionKey);
    }

    @Override
    public User getUserByName(String userName) {
        if (!userNameUser.containsKey(userName)) {
            return null;
        }
        return userNameUser.get(userName);
    }

    @Override
    public Date getSessionBeginDate(String sessionKey) {
        if (!sessionTiming.containsKey(sessionKey)) {
            return null;
        }
        return sessionTiming.get(sessionKey);
    }

    @Override
    public boolean removeSession(String sessionKey) {
        User user = sessions.remove(sessionKey);
        User nUser = new User(user.getUsername(), user.getPassword(), false, true, true, true, user.getAuthorities());
        userNameUser.replace(user.getUsername(), nUser);
        sessionTiming.remove(sessionKey);
        userSessionMap.remove(user.getUsername());
        return true;
    }

    @Override
    public boolean addSession(String sessionKey, String userName) {
        if(!userNameUser.containsKey(userName))
            return false;
        User user = userNameUser.get(userName);
        user = new User(user.getUsername(), user.getPassword(), true, true, true, true, user.getAuthorities());
        userNameUser.replace(userName, user);
        sessionTiming.put(sessionKey, new Date());
        sessions.put(sessionKey, user);
        userSessionMap.put(userName, sessionKey);
        return true;
    }

    @Override
    public boolean addUser(String userName, String password, String email, String[] args) {
        Collection<GrantedAuthority> userAuthorities = new ArrayList<GrantedAuthority>();
        userAuthorities.add(new RandomUser());

        User user = new User(userName,String.valueOf(password.hashCode()), false, true, true, true, userAuthorities);
        emails.add(email);
        users.add(userName);
        userNameUser.put(userName, user);


        return true;
    }
}
