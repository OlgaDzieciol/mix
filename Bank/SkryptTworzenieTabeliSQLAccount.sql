create table Account (
   id INT NOT NULL identity,
   userId     INT  default NULL,
   accountNumber VARCHAR(26) default NULL,
   accountType     INT  default NULL,
  -- username VARCHAR(20) default NULL,
   --password VARCHAR(20) default NULL,
  -- email VARCHAR(150) default NULL,
  -- pesel VARCHAR(11) default NULL,
  
 --  birthDate DATE default null,

   balance     DOUBLE PRECISION  default 0,
 --  isEnabled BIT default 0,
   PRIMARY KEY (id)
);