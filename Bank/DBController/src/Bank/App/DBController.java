package Bank.App;

import Bank.App.Contracts.IAccount;
import Bank.App.Contracts.ICredit;
import Bank.App.Contracts.IHistory;
import Bank.App.Contracts.IUserData;
import Bank.App.Models.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Jan on 2015-01-08.
 */
@Component
public class DBController implements IAccount,IHistory,IUserData,ICredit{
    private static SessionFactory factory;

    public DBController() {
        try{
            factory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }

    }


    /* CARD METHODS*/
    @Override public Integer addCard(String cardnumber, int pinCode, int accountId, double dayLimit, boolean isActive){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer cardId = null;
        try{
            tx = session.beginTransaction();
            Card card = new Card(cardnumber,pinCode,dayLimit,pinCode,isActive);
            cardId = (Integer) session.save(card);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return cardId;
    }
    @Override public void updateCard(Card newCard ){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            //Card oldcard = (Card)session.get(Card.class, newCard.getId());
            //oldcard = newCard;
            session.update(newCard);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    @Override public Card getCard(String cardnumber)   {
        Session session = factory.openSession();
        Transaction tx = null;
        Card card = null;
        try{
            tx = session.beginTransaction();
            card = (Card)session.createQuery("FROM Card where cardNumber = ' " + cardnumber + "'").list().get(0);
            tx.commit();
        }
        catch (HibernateException e){
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();

        }
        return card;

    }
    public List<Card> getCards(String querySQL ){
        Session session = factory.openSession();
        Transaction tx = null;
        List cards = null;
        try{
            tx = session.beginTransaction();
            cards = session.createQuery(querySQL).list();
            tx.commit();

        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return cards;
    }


    /* ACCOUNT METHODS*/
    public Integer addAccount(int userId, String accountNumber, double balance, int type){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer accId = null;
        try{
            tx = session.beginTransaction();
            Account acc = new  Account( userId,  accountNumber,  balance,  type) ;
            accId = (Integer) session.save(acc);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return accId;
    }
    public void updateCAccountBalance(int accountId, double newBalance ){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Account account = (Account)session.get(Account.class, accountId);
            account.setBalance(newBalance);
            session.update(account);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    @Override public List<Account> getUserAccounts(int userId ){
        Session session = factory.openSession();
        Transaction tx = null;
        List accounts = null;
        try{
            tx = session.beginTransaction();
            accounts = session.createQuery("FROM Account where userId =" + userId).list();
            tx.commit();

        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return accounts;
    }
    public Account getAccount(Integer accountId)  {
        Session session = factory.openSession();
        Transaction tx = null;
        Account accounts = null;
        try{
            tx = session.beginTransaction();
            accounts = (Account) session.get(Account.class, accountId);
            tx.commit();

        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return accounts;
    }
    @Override public Account getAccount(String accountNumber) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Account> accounts = null;
        Account acc = null;
        try{
            tx = session.beginTransaction();
            accounts =  session.createQuery("FROM Account where accountNumber = '" + accountNumber + "'").list();
            if(accounts.size() != 0){
                acc = accounts.get(0);
            }
            tx.commit();

        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return acc;
    }
    @Override public void addAccount(Account newAccount) {
        this.addAccount(newAccount.getUserId(),newAccount.getAccountNumber(),newAccount.getBalance(),newAccount.getAccountType());
    }
    @Override public boolean UpdateAccountInfo(String accNumber, Account newAccountInfo) {
        return false;
    }
    @Override public void updateAccountBalance(int id, double newBalance) {
        this.updateCAccountBalance(id, newBalance);
    }
    @Override public boolean CheckIfExists(String accNumber) {
        Account acc = this.getAccount(accNumber);
        if (acc!=null){
            return true;
        }else{
            return false;
        }
    }


    /* TRANSFER METHODS*/
    private Integer addTransfer(String accountNumberFrom, String accountNumberTo, Date transferDate, String title, double dollarCount, boolean isRealized, boolean isScheduled, int scheduleDay){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer transferId = null;
        try{
            tx = session.beginTransaction();
            Transfer transfer = new  Transfer( accountNumberFrom,  accountNumberTo,  transferDate,  title,  dollarCount,  isRealized,  isScheduled,  scheduleDay) ;
            transferId = (Integer) session.save(transfer);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return transferId;
    }
    private Integer AddTransfer(Transfer transfer){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer transferId = null;
        try{
            tx = session.beginTransaction();
            //Transfer transfer = new  Transfer( accountNumberFrom,  accountNumberTo,  transferDate,  title,  dollarCount,  isRealized,  isScheduled,  scheduleDay) ;
            transferId = (Integer) session.save(transfer);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return transferId;
    }
    @Override public void updateTransfer(Transfer transfer){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            //Card oldcard = (Card)session.get(Card.class, newCard.getId());
            //oldcard = newCard;
            session.update(transfer);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    private void deleteTransfer(int transferId){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Transfer transfer =
                    (Transfer)session.get(Transfer.class, transferId);
            session.delete(transfer);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    @Override public void addTransfer(Transfer transfer) {
        if (!transfer.getisScheduled()){
            this.AddTransfer(transfer);
        }
        else{
            this.addToSchedule(transfer);
        }
    }
    @Override public boolean deleteTransfer(int id, boolean isScheduled) {
        if (!isScheduled){
            this.deleteTransfer(id);
        }
        else{
            this.deleteSchedule(id);
        }
        return true;
    }
    @Override public List<Transfer> getScheduledTransfers(int scheduleDay ){
        Session session = factory.openSession();
        Transaction tx = null;
        List transfers = null;
        try{
            tx = session.beginTransaction();
            transfers = session.createQuery("FROM ScheduledTransfer where isScheduled = 1 and scheduleDay =  " + scheduleDay).list();
            tx.commit();
            if(transfers.size() != 0){
                System.out.print("Znaleziono zaplanowane przelewy \n");
            }else{
                System.out.print("Nie znaleziono zaplanowanych przelewow \n");
            }

        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return transfers;
    }
    @Override public List<Transfer> getNonRealizedTransfers() {
        Session session = factory.openSession();
        Transaction tx = null;
        List transfers = null;
        try{
            tx = session.beginTransaction();
            transfers = session.createQuery("FROM Transfer where isRealized = 0 ").list();
            tx.commit();

        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return transfers;
    }
    @Override public List<Transfer> getTransferHistory(String accountNumberFrom ){
        Session session = factory.openSession();
        Transaction tx = null;
        List<Transfer> transfers = null;
        try{
            tx = session.beginTransaction();
            transfers = session.createQuery("FROM Transfer where accountNumberFrom = '" + accountNumberFrom+"'").list();
            tx.commit();

        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        //Taki tam hack bo nie przekazuje stąd wartości listy przez interfejs
//        if(transfers.size() != 0){
//            for(Transfer transfer : transfers){
//                System.out.print("Nr konta odbiorcy : " + transfer.getAccountNumberTo() + "\n");
//                System.out.print("Kwota przelewu : " + transfer.getDollarCount() + "\n");
//                System.out.print("Tytuł przelewu : " + transfer.getTitle() + "\n");
//                System.out.print("Czy zrealizowany : " + transfer.getisRealized() + "\n");
//            }
//        }else{
//            System.out.print("Nie masz zadnych przelewow");
//        }
        return transfers;
    }



    /* BANKUSER METHODS*/
    private Integer addBankUser(String firstname, String lastname, String username, String password, String email, String pesel, Date birthDate, boolean isEnabled){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer bankUserId = null;
        try{
            tx = session.beginTransaction();
            BankUser bankUser = new  BankUser( firstname,  lastname,  username,  password,  email,  pesel,  birthDate,  isEnabled) ;
            bankUserId = (Integer) session.save(bankUser);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return bankUserId;
    }
    private Integer addBankUser(BankUser bankUser){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer bankUserId = null;
        try{
            tx = session.beginTransaction();
            //BankUser bankUser = new  BankUser( firstname,  lastname,  username,  password,  email,  pesel,  birthDate,  isEnabled) ;
            bankUserId = (Integer) session.save(bankUser);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return bankUserId;
    }
    @Override public BankUser getUser(String username)  {
        Session session = factory.openSession();
        Transaction tx = null;
        BankUser user = null;
        try{
            tx = session.beginTransaction();
            List<BankUser> userList = session.createQuery("FROM BankUser where username = '" + username + "'").list();
            if (userList.size() != 0){
                user = userList.get(0);
            }
            tx.commit();

        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return user;
    }
    @Override public BankUser getUser(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        BankUser card = null;
        try{
            tx = session.beginTransaction();
            card = (BankUser)session.get(BankUser.class, id);
            tx.commit();
        }
        catch (HibernateException e){
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();

        }
        return card;
    }
    @Override public String updateUser(BankUser newUser) {
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            //Card oldcard = (Card)session.get(Card.class, newCard.getId());
            //oldcard = newCard;
            session.update(newUser);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            System.out.print(e.toString());
        }finally {
            session.close();
        }
        return "Sukces \n";
    }
    @Override public boolean userExist(String username) {
        if (this.getUser(username) != null){
            return true;
        }
        else{
            return false;
        }
    }
    @Override public boolean emailUsed(String email) {
        Session session = factory.openSession();
        Transaction tx = null;
        BankUser user = null;
        try{
            tx = session.beginTransaction();
            //user = (BankUser) session.createQuery("FROM BankUser where email =" + email).list().get(0);
            List<BankUser> userList = session.createQuery("FROM BankUser where email = '" + email + "'").list();
            if (userList.size() != 0){
                user = userList.get(0);
            }
            tx.commit();

        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        if (user != null){
            return true;
        }
        else{
            return false;
        }
    }
    @Override public String getPassword(String username) {
        String pass = this.getUser(username).getPassword();
        return pass;
    }
    @Override public int addUser(BankUser newUser) {
        int id = this.addBankUser(newUser);
        return id;
    }


    /* SCHEDULE METHODS */

    private void addToSchedule(Transfer transfer){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer transferId = null;
        try{
            tx = session.beginTransaction();
            ScheduledTransfer local = new ScheduledTransfer(transfer.getAccountNumberFrom(),transfer.getAccountNumberTo(),transfer.getTransferDate(),transfer.getTitle(),transfer.getDollarCount(),transfer.getisRealized(),transfer.getisScheduled(),transfer.getScheduleDay());
            transferId = (Integer) session.save(local);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    private void deleteSchedule(int transferId){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Transfer transfer =
                    (ScheduledTransfer)session.get(ScheduledTransfer.class, transferId);
            session.delete(transfer);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }


    @Override
    public void newCredit(Credit credit) {
        Session session = factory.openSession();
        Transaction tx = null;
        Integer transferId = null;
        try{
            tx = session.beginTransaction();
            transferId = (Integer) session.save(credit);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }

    }

    @Override
    public Credit getCredit(int userId) {
        Session session = factory.openSession();
        Transaction tx = null;
        Credit credit = null;
        try{
            tx = session.beginTransaction();
            List<Credit> creditList =  session.createQuery("FROM Credit where userId =" + userId).list();
            if(creditList.size() != 0){
                credit = creditList.get(0);
            }
            tx.commit();

        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return credit;
    }
    /* Test methods, unused model employee */


    /* Method to CREATE an employee in the database */
    public Integer addEmployee(String fname, String lname, int salary){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer employeeID = null;
        try{
            tx = session.beginTransaction();
            Employee employee = new Employee(fname, lname, salary);
            employee.setId(3);
            employeeID = (Integer) session.save(employee);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return employeeID;
    }
    /* Method to  READ all the employees */
    public void listEmployees( ){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            List employees = session.createQuery("FROM Employee").list();
            for (Iterator iterator =
                         employees.iterator(); iterator.hasNext();){
                Employee employee = (Employee) iterator.next();
                System.out.print("First Name: " + employee.getFirstName());
                System.out.print("  Last Name: " + employee.getLastName());
                System.out.println("  Salary: " + employee.getSalary());
            }
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    /* Method to UPDATE salary for an employee */
    public void updateEmployee(Integer EmployeeID, int salary ){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Employee employee =
                    (Employee)session.get(Employee.class, EmployeeID);
            employee.setSalary( salary );
            session.update(employee);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    /* Method to DELETE an employee from the records */
    public void deleteEmployee(Integer EmployeeID){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Employee employee =
                    (Employee)session.get(Employee.class, EmployeeID);
            session.delete(employee);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }



}
