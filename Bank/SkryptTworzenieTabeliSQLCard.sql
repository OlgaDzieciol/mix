create table Card (
   id INT NOT NULL identity,
   cardNumber VARCHAR(26) default NULL,
   pinCode     INT  default NULL,
   accountId     INT  default NULL,
  -- username VARCHAR(20) default NULL,
   --password VARCHAR(20) default NULL,
  -- email VARCHAR(150) default NULL,
  -- pesel VARCHAR(11) default NULL,
  
 --  birthDate DATE default null,

   dayLimit     DOUBLE PRECISION  default 0,
   isActive BIT default 0,
   PRIMARY KEY (id)
);