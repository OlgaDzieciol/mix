package Bank.App.Contracts;
import Bank.App.Models.Card;
/**
 * Created by Olga on 2014-12-16.
 */
public interface ICards {
    Card OrderNewCard(String accountNumber);
    void SetNewPincode(String cardNumber,int newCode);
    void SetNewLimit(String cardNumber,double dayLimit);
    String ActivateCard(String cardNumber);


}
