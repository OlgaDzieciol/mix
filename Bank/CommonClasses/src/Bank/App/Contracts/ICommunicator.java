package Bank.App.Contracts;

import javax.mail.MessagingException;
import java.util.List;


/**
 * Created by Mateusz on 2014-11-24.
 * Zmienna remindForMe - nie pamiętam do czego była, nie chcę jej pochopnie usunąć - do ustalenia
 * Designed to be implemented as threadsafe Singletone: http://en.wikipedia.org/wiki/Initialization-on-demand_holder_idiom
 */
public interface ICommunicator {
    void sendEmail(String senderAddress, String recipientAddress, String title, String textOfMessage, List<AttachmentDetails> attachmentList) throws MessagingException;
    void sendRecoveryLink(String emailAddress, String senderAddress, String recoveryLink);
}
