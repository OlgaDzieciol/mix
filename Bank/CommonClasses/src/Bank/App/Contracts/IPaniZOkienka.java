package Bank.App.Contracts;

import Bank.App.Models.*;

import java.sql.Date;
import java.util.List;

/**
 * Created by Olga on 2014-12-16.
 */


public interface IPaniZOkienka {

    // logowanie
    boolean Zaloguj(String username, String password);

    //rejestracja
    BankUser Rejestruj(String username, String email, String password);
    BankUser getUsername(String username);



    //wysyłanie wiadomosci
    void Wyslij(String messageTitle, String messageText,int id);

    List<Account> wyswietlKonta(int userId);
    List<Transfer> wyswietlPrzelewy(String accNumber);
    //wyswietlanie wiadomosci odebrane i wysłane
    List Wyswietl(String messageText, int id);

    //edycja danych
    String EdytujDane(BankUser uzytkownik);

    //zmiana hasła
    String ZmienHaslo(int id, String newPassword);

    //dane o koncie
    Account Konto(String accountNumber, double balance, int accountType, int userId);

    //robienie przelewów i zlecen stałych
    String transfer(Transfer transfer);


    //rachunek oszczędnościowy --> Konto()

    Account ZalozKonto(int userid, int accountType, double balance, Date birthDate, int czyLokata); //howLong - długośc lokaty w miesiącach(np), procent zawsze 3

    //nowa karta
    String aktywujKarte(String cardNumber);
    Card zamowKarte(String accountNumber);
    Card ustawLimit( String cardNumber, double dayLimit);

    int ZmienPIN(String cardNumber, int newPIN);

    void WymusRealizacjePrzelewow();

    void newCredit(Credit credit);
    Credit getCredit(int userId);
}
