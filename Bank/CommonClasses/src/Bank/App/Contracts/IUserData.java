package Bank.App.Contracts;

import Bank.App.Models.BankUser;

/**
 * Created by Olga on 2014-12-16.
 */
public interface IUserData {
    boolean userExist(String username);
    boolean emailUsed(String email);
    String getPassword(String username);
    int addUser(BankUser newUser);
    BankUser getUser(String username);
    BankUser getUser(int id);
    String updateUser(BankUser newUser);
}
