package Bank.App.Contracts;

import javax.mail.internet.MimeBodyPart;

/**
 * Created by Mateusz on 2014-11-24.
 */
public class AttachmentDetails {
    public String fileName;
    public MimeBodyPart attachmentBody;
}
