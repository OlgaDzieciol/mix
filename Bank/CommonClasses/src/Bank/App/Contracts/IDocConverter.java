package Bank.App.Contracts;

import Bank.App.Models.Transfer;

import java.io.File;

/**
 * Created by Olga on 2014-12-16.
 */
public interface IDocConverter {
    /*exception InvalidDocumentFormat;*/
    org.apache.pdfbox.pdmodel.PDDocument XMLToPDF(File XMLDoc, String outputPath) throws InvalidDocumentFormatException, InvalidDocumentFormatException;
    AttachmentDetails getMailDocument(Transfer transfer);

}
