package Bank.App.Contracts;

import Bank.App.Models.InsufficientFundsException;
import Bank.App.Models.Transfer;

import java.util.Date;
import java.util.List;

/**
 * Created by Olga on 2014-12-16.
 */
public interface ITransfer {
    /*exception InsufficientFundsException;*/
    void Transfer(Transfer transfer) throws InsufficientFundsException;
    boolean ScheduleTransfer(Transfer transfer, int day);
    boolean RemoveScheduledTransfer(int transferId);
    List<Transfer> getTransferHistory(String accountNumberFrom);
    void realizeTransfersForDay(Date date);


}


