package Bank.App.Contracts;

import Bank.App.Models.Transfer;

import java.util.List;

/**
 * Created by Olga on 2014-12-16.
 */
public interface IHistory {
   /* exception NoSuchObjectInDataBaseException;*/
    void addTransfer(Transfer transfer);
    boolean deleteTransfer(int id, boolean isScheduled);
    List<Transfer> getTransferHistory(String accountNumberFrom);
    List<Transfer> getScheduledTransfers(int day);
    List<Transfer> getNonRealizedTransfers();
 void updateTransfer(Transfer transfer);

}
