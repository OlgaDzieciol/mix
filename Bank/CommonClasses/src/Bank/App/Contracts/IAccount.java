/**
 * Created by Olga on 2014-12-16.
 */

package Bank.App.Contracts;
import Bank.App.Models.Account;
import Bank.App.Models.Card;

import java.util.List;

public interface IAccount {
    Account  getAccount(String accNumber);
    void addAccount(Account newAccount);
    boolean UpdateAccountInfo(String accNumber, Account newAccountInfo);
    void updateAccountBalance(int id, double newBalance);
    boolean CheckIfExists(String accNumber);

    public Integer addCard(String cardnumber, int pinCode, int accountId, double dayLimit, boolean isActive);
    public void updateCard(Card newCard );
    public Card getCard(String cardnumber);
    public List<Account> getUserAccounts(int userId );


}
