package Bank.App.Contracts;

import Bank.App.Models.Credit;

/**
 * Created by Olga on 2014-12-16.
 */
public interface ICredit {
    void newCredit(Credit credit);
    Credit getCredit(int userId);

}
