package Bank.App.Models;

/**
 * Created by olga on 2014-12-30.
 */
public class Credit {
    private int id;
    private int userId;
    private double creditLeft;

    public Credit() {

    }

    public Credit(int userId, double creditLeft) {
        this.userId = userId;
        this.creditLeft = creditLeft;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getCreditLeft() {
        return creditLeft;
    }

    public void setCreditLeft(double creditLeft) {
        this.creditLeft = creditLeft;
    }
}
