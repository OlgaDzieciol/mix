package Bank.App.Models;

/**
 * Created by Jan on 2015-01-01.
 */
public class InsufficientFundsException extends Exception {

    public String message = "Brak funduszy na koncie";

}
