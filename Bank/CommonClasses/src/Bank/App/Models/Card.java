package Bank.App.Models;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by olga on 2014-12-30.
 */
@Entity
@Table(name = "dbo.Card")
public class Card {
    int id;


    String cardNumber;
    int accountId;
    double dayLimit;
    int pinCode;
    boolean isActive;
    public Card(){};
    public Card(String cardNumber, int accountId, double dayLimit, int pinCode, boolean isActive) {
        this.cardNumber = cardNumber;
        this.accountId = accountId;
        this.dayLimit = dayLimit;
        this.pinCode = pinCode;
        this.isActive = isActive;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public double getDayLimit() {
        return dayLimit;
    }

    public void setDayLimit(double dayLimit) {
        this.dayLimit = dayLimit;
    }

    public int getPinCode() {
        return pinCode;
    }

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode;
    }

    public boolean getisActive() {
        return isActive;
    }

    public void setisActive(boolean isActive) {
        this.isActive = isActive;
    }

}
