package Bank.App.Models;

import java.sql.Date;

/**
 * Created by Jan on 2015-01-11.
 */
public class ScheduledTransfer extends Transfer {
    private int id;
    String accountNumberFrom;
    String accountNumberTo;
    Date transferDate;
    String title;
    double dollarCount;
    boolean isRealized;
    boolean isScheduled;
    Integer scheduleDay;

    public ScheduledTransfer(){}

    public ScheduledTransfer(String accountNumberFrom, String accountNumberTo, Date transferDate, String title, double dollarCount, boolean isRealized, boolean isScheduled, Integer scheduleDay) {
        this.accountNumberFrom = accountNumberFrom;
        this.accountNumberTo = accountNumberTo;
        this.transferDate = transferDate;
        this.title = title;
        this.dollarCount = dollarCount;
        this.isRealized = isRealized;
        this.isScheduled = isScheduled;
        this.scheduleDay = scheduleDay;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccountNumberFrom() {
        return accountNumberFrom;
    }

    public void setAccountNumberFrom(String accountNumberFrom) {
        this.accountNumberFrom = accountNumberFrom;
    }

    public String getAccountNumberTo() {
        return accountNumberTo;
    }

    public void setAccountNumberTo(String accountNumberTo) {
        this.accountNumberTo = accountNumberTo;
    }

    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getDollarCount() {
        return dollarCount;
    }

    public void setDollarCount(double dollarCount) {
        this.dollarCount = dollarCount;
    }

    public boolean getisRealized() {
        return isRealized;
    }

    public void setisRealized(boolean isRealized) {
        this.isRealized = isRealized;
    }

    public boolean getisScheduled() {
        return isScheduled;
    }

    public void setisScheduled(boolean isScheduled) {
        this.isScheduled = isScheduled;
    }

    public Integer getScheduleDay() {
        return scheduleDay;
    }

    public void setScheduleDay(Integer scheduleDay) {
        this.scheduleDay = scheduleDay;
    }

    /* typedef sequence<String> emailAddress; */

}
