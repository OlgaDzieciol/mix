package Bank.App.Models;

/**
 * Created by Jan on 2015-01-09.
 */
public enum AccountType {
    BASIC(0),
    SAVER(1),
    INVESTMENT(2);

    private int value;
    private AccountType(int value)
    {
        this.value = value;
    }

    public int getValue(){
        return value;
    }
}
