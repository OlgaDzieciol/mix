create table BankUser (
   id INT NOT NULL identity,
   --userId     INT  default NULL,
   name VARCHAR(20) default NULL,
   surname VARCHAR(20) default NULL,
   username VARCHAR(20) default NULL,
   password VARCHAR(20) default NULL,
   email VARCHAR(20) default NULL,
   pesel VARCHAR(20) default NULL,
  
   birthDate DATE default null,

   dollarCount     DOUBLE PRECISION  default 0,
   isEnabled BIT default 0,
   PRIMARY KEY (id)
);