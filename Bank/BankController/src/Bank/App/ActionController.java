package Bank.App;

import Bank.App.Contracts.*;
import Bank.App.Models.*;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by olga on 2014-12-30.
 */
@Component
public class ActionController implements IPaniZOkienka {

    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    ITransfer transferProvider;
    @Autowired
    ICards cardsProvider;
    @Autowired
    IAccount accountProvider;
    @Autowired
    IUserData userProvider;
    @Autowired
    ICommunicator emailProvider;
    @Autowired
    ICredit creditProvider;
    public ActionController(){
    }
    public void setApplicationContext(
            ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
    @Override
    public String transfer(Transfer transfer) {
       if (transfer.getisScheduled()){
           if(transferProvider.ScheduleTransfer(transfer, transfer.getScheduleDay()))
           {
               return "Pomyślnie zaplanowano przelew\n";
           }
           else{
               return "Nie udało się zaplanować przelewu\n";
           }
       }else{
           Account accTo = accountProvider.getAccount(transfer.getAccountNumberTo());
           if(accTo!=null) {
               try {
                   transferProvider.Transfer(transfer);
               } catch (InsufficientFundsException e) {
                   return "Brak środków na koncie\n";
               }
               return "Pomyślnie udało się zrealizować przelew\n";
           }else{
               return "Podany nr konta nie istnieje \n";
           }
       }

    }

    @Override
    public BankUser getUsername(String username) {
        return userProvider.getUser(username);
    }
    @Override
    public boolean Zaloguj(String username, String password) {
        String dbPass = userProvider.getPassword(username);
        if (password.equals(dbPass))
        {
            return true;
        }
        else{
            return false;
        }
    }


    @Override
    public BankUser Rejestruj(String username, String email, String password) {
        BankUser bankUser = null;
        if (!userProvider.userExist(username) && !userProvider.emailUsed(email)){
         bankUser = new BankUser(null,null,username,password,email,null,null,true);
        bankUser.setId(userProvider.addUser(bankUser));}
        return bankUser;
    }



    @Override
    public void Wyslij(String messageTitle, String messageText, int id) {
        //TODO Wiadomosci
    }

    @Override
    public List<Account> wyswietlKonta(int userId) {
        List<Account> list = accountProvider.getUserAccounts(userId);
        return list;
    }

    @Override
    public List<Transfer> wyswietlPrzelewy(String accNumber) {
        List<Transfer> local = transferProvider.getTransferHistory(accNumber);

        return local;
    }

    @Override
    public List Wyswietl(String messageText, int id) {
        //TODO Wiadomosci
        return null;
    }

    @Override
    public String EdytujDane(BankUser uzytkownik) {
        String msg = userProvider.updateUser(uzytkownik);
        return msg;
    }

    @Override
    public String ZmienHaslo(int id, String newPassword) {
      try {
          BankUser user = userProvider.getUser(id);
          user.setPassword(newPassword);
          userProvider.updateUser(user);
      }catch(Exception e){
          return "Wystąpił błąd";
      }
        return "Pomyślnie zmieniono hasło";
    }

    @Override
    public Account Konto(String accountNumber, double balance, int accountType, int userId) {
        return null;
    }
//TODO System działa jedynie brakuje workerow. Potrzebny jeden do wywoływania realizacji przelewów, jeden do naliczania zwrotu z lokaty, do naliczania kredytu oraz sprawdzający kredyt/lokatę czy już należy wypłacić




    @Override
    public Account ZalozKonto(int id, int accountType, double balance, Date birthDate, int howLong) {

        Account account = new Account(id,newAccNumber(), balance,accountType);
        accountProvider.addAccount(account);
        return account;
    }
    private String newAccNumber(){
        String accountNumber;
        do {
            accountNumber = "10101010101010101010";
            Random random = new Random();
            for (int i = 0; i < 6; i++) {
                accountNumber+= random.nextInt(10);
            }
        }while(accountProvider.CheckIfExists(accountNumber));


       return accountNumber;
    }

    @Override
    public String aktywujKarte(String cardNumber) {
        String msg = cardsProvider.ActivateCard(cardNumber);
        return msg;
    }

    @Override
    public Card zamowKarte(String accountNumber) {
        Card card = cardsProvider.OrderNewCard(accountNumber);
        return card;
    }

    @Override
    public Card ustawLimit( String cardNumber, double dayLimit) {

        cardsProvider.SetNewLimit(cardNumber,dayLimit);
        return null;
    }

    @Override
    public int ZmienPIN(String cardNumber, int newPIN) {
        cardsProvider.SetNewPincode(cardNumber, newPIN);
        return 1;
    }

    @Override
    public void WymusRealizacjePrzelewow() {
        transferProvider.realizeTransfersForDay(new Date(System.currentTimeMillis()));
    }

    @Override
    public void newCredit(Credit credit) {
        creditProvider.newCredit(credit);
    }

    @Override
    public Credit getCredit(int userId) {
        return creditProvider.getCredit(userId);
    }

}
