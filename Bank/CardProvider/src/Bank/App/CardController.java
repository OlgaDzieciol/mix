package Bank.App;

import Bank.App.Contracts.IAccount;
import Bank.App.Contracts.ICards;
import Bank.App.Models.Card;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * Created by olga on 2014-12-30.
 */
@Component
public class CardController implements ICards {
    @Autowired
    IAccount accountProvider;

    @Override
    public Card OrderNewCard(String accountNumber) {
        if (accountProvider.CheckIfExists(accountNumber)) {

            Card card = new Card(newCardNumber(), accountProvider.getAccount(accountNumber).getId(), 2000, 1234, false);
            card.setId(accountProvider.addCard(card.getCardNumber(), card.getPinCode(), card.getAccountId(), card.getDayLimit(), card.getisActive()));
            return card;
        } else {
            return null;
        }

    }

    private String newCardNumber() {
        String accountNumber;
        do {
            accountNumber = "10101010101010101010";
            Random random = new Random();
            for (int i = 0; i < 6; i++) {
                accountNumber += random.nextInt(10);
            }
        } while (accountProvider.CheckIfExists(accountNumber));


        return accountNumber;
    }

    @Override
    public void SetNewPincode(String cardNumber, int newCode) {
        Card local = accountProvider.getCard(cardNumber);
        if (local != null) {
            local.setPinCode(newCode);
            accountProvider.updateCard(local);
        }
    }

    @Override
    public void SetNewLimit(String cardNumber, double dayLimit) {

        Card local = accountProvider.getCard(cardNumber);
        if (local != null) {
            local.setDayLimit(dayLimit);
            accountProvider.updateCard(local);
        }
    }

    @Override
    public String ActivateCard(String cardNumber) {
        Card local = accountProvider.getCard(cardNumber);
        if (local != null) {
            local.setisActive(true);
            accountProvider.updateCard(local);
            return "Sukces";
        }
        else{
            return  "Brak karty o podanym numerze";
        }
    }
}
